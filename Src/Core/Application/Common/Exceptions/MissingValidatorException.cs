﻿namespace Application.Common.Exceptions;

public sealed class MissingValidatorException<T> : Exception
{
    public MissingValidatorException()
        : base($"Validator is missing for type: {typeof(T).FullName}")
    { }
}