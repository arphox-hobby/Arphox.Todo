﻿using FluentValidation;

namespace Application.TodoItems.Commands.Common;

internal static class CommonTodoItemValidators
{
    internal static IRuleBuilderOptions<T, string> ValidateTodoItemTitle<T>(
        IRuleBuilder<T, string> ruleBuilder)
    {
        return ruleBuilder
            .NotEmpty()
            .MaximumLength(5000);
    }
}
