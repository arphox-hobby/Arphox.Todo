﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.TodoItems.Commands.UpdateTodoItem;

public class UpdateTodoItemCommandHandler : IRequestHandler<UpdateTodoItemCommand, TodoItem>
{
    private readonly ITodoDbContext _context;

    public UpdateTodoItemCommandHandler(ITodoDbContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task<TodoItem> Handle(UpdateTodoItemCommand request, CancellationToken cancellationToken)
    {
        var item = await TryGetItem(request, cancellationToken);
        if (item == null)
            throw new NotFoundException<TodoItem>(request.Id);

        Update(request, item);
        await _context.SaveChangesAsync(cancellationToken);
        return item;
    }

    private static void Update(UpdateTodoItemCommand request, TodoItem item)
    {
        item.Title = request.Title;
        item.IsComplete = request.IsComplete;
    }

    private ValueTask<TodoItem?> TryGetItem(UpdateTodoItemCommand request, CancellationToken cancellationToken)
    {
        return _context.TodoItems.FindAsync(
            keyValues: new object?[] { request.Id },
            cancellationToken: cancellationToken);
    }
}
