﻿using FluentValidation;
using static Application.TodoItems.Commands.Common.CommonTodoItemValidators;

namespace Application.TodoItems.Commands.UpdateTodoItem;

public sealed class UpdateTodoItemCommandValidator : AbstractValidator<UpdateTodoItemCommand>
{
    public UpdateTodoItemCommandValidator()
    {
        ValidateTodoItemTitle(RuleFor(x => x.Title));
    }
}
