﻿using Domain.Entities;
using MediatR;

namespace Application.TodoItems.Commands.UpdateTodoItem;

/// <remarks>Handler: <see cref="UpdateTodoItemCommandHandler"/></remarks>
public sealed class UpdateTodoItemCommand : IRequest<TodoItem>
{
    public long Id { get; set; }

    public string Title { get; set; }

    public bool IsComplete { get; set; }
}
