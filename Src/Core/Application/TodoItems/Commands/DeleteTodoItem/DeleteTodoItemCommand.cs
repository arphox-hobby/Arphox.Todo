﻿using MediatR;

namespace Application.TodoItems.Commands.DeleteTodoItem;

/// <remarks>Handler: <see cref="DeleteTodoItemCommandHandler"/></remarks>
public sealed class DeleteTodoItemCommand : IRequest
{
    public long Id { get; set; }
}
