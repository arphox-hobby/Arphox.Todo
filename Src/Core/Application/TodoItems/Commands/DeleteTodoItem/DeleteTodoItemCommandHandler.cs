﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.TodoItems.Commands.DeleteTodoItem;

public sealed class DeleteTodoItemCommandHandler : IRequestHandler<DeleteTodoItemCommand>
{
    private readonly ITodoDbContext _context;

    public DeleteTodoItemCommandHandler(ITodoDbContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task<Unit> Handle(DeleteTodoItemCommand request, CancellationToken cancellationToken)
    {
        var item = await TryGetItem(request, cancellationToken);
        if (item == null)
            throw new NotFoundException<TodoItem>(request.Id);

        _context.TodoItems.Remove(item);
        await _context.SaveChangesAsync(cancellationToken);
        return Unit.Value;
    }

    private ValueTask<TodoItem?> TryGetItem(DeleteTodoItemCommand request, CancellationToken cancellationToken)
    {
        return _context.TodoItems.FindAsync(
            new object?[] { request.Id },
            cancellationToken);
    }
}
