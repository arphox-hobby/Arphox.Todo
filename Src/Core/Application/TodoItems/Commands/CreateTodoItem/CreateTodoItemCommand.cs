﻿using MediatR;

namespace Application.TodoItems.Commands.CreateTodoItem;

/// <remarks>Handler: <see cref="CreateTodoItemCommandHandler"/></remarks>
public sealed class CreateTodoItemCommand : IRequest<long>
{
    public string Title { get; set; }
}