﻿using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using System.Diagnostics;

namespace Application.TodoItems.Commands.CreateTodoItem;

internal sealed class CreateTodoItemCommandHandler : IRequestHandler<CreateTodoItemCommand, long>
{
    private readonly ITodoDbContext _context;

    public CreateTodoItemCommandHandler(ITodoDbContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task<long> Handle(CreateTodoItemCommand request, CancellationToken cancellationToken)
    {
        var entity = new TodoItem
        {
            Title = request.Title
        };

        _context.TodoItems.Add(entity);

        int affectedCount = await _context.SaveChangesAsync(cancellationToken);
        if (affectedCount != 1)
        {
            // TODO
            Debugger.Break();
        }

        return entity.Id;
    }
}