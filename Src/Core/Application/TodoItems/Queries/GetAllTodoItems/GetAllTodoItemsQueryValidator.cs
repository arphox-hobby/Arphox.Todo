﻿using FluentValidation;

namespace Application.TodoItems.Queries.GetTodoItem;

public sealed class GetAllTodoItemsQueryValidator : AbstractValidator<GetAllTodoItemsQuery>
{
}
