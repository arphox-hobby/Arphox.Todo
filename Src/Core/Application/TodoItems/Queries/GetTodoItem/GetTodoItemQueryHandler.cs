﻿using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.TodoItems.Queries.GetTodoItem;

internal sealed class GetTodoItemQueryHandler : IRequestHandler<GetTodoItemQuery, TodoItem>
{
    private readonly ITodoDbContext _context;

    public GetTodoItemQueryHandler(ITodoDbContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task<TodoItem> Handle(GetTodoItemQuery request, CancellationToken cancellationToken)
    {
        var item = await _context.TodoItems.FindAsync(
            keyValues: new object?[] { request.Id },
            cancellationToken: cancellationToken);

        if (item == null)
            throw new NotFoundException<TodoItem>(request.Id);

        return item;
    }
}
