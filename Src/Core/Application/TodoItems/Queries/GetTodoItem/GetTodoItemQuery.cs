﻿using Domain.Entities;
using MediatR;

namespace Application.TodoItems.Queries.GetTodoItem;

/// <remarks>Handler: <see cref="GetTodoItemQueryHandler"/></remarks>
public sealed class GetTodoItemQuery : IRequest<TodoItem>
{
    public long Id { get; set; }
}