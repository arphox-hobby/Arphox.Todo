﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Application.Common.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Persistence;

public static class DependencyInjection
{
    public static IServiceCollection AddPersistence(
        this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddEntityFrameworkNpgsql()
            .AddDbContextPool<TodoDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("TodoDatabase")));

        services.AddScoped<ITodoDbContext>(
            provider => provider.GetRequiredService<TodoDbContext>());

        return services;
    }
}
