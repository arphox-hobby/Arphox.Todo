﻿using HotChocolate.Execution;

namespace WebApi.GraphQL;

public static class GraphQLsetup
{
    public static void SetupGraphQL(this IServiceCollection services)
    {
        services
            .AddGraphQLServer()
            .ModifyOptions(options =>
            {
                options.DefaultResolverStrategy = ExecutionStrategy.Serial;
            })
            .AddQueryType<Query>()
            .AddProjections()
            .AddFiltering()
            .AddSorting();
    }
}
