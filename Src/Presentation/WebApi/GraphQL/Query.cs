﻿using Persistence;
using Domain.Entities;

namespace WebApi.GraphQL;

public class Query
{
    [Serial]
    [UsePaging]
    [UseProjection]
    [UseFiltering]
    [UseSorting]
    public IQueryable<TodoItem> GetTodoItems([Service] TodoDbContext context)
    {
        return context.TodoItems;
    }
}
