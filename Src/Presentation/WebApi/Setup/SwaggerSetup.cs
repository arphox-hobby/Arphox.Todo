﻿using Microsoft.OpenApi.Models;

namespace WebApi.Setup;

public static class SwaggerSetup
{
    private const string Version = "v1";

    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        return services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc(Version, new OpenApiInfo
            {
                Version = Version,
                Title = "Arphox.Todo WebApi",
                Description = "WebApi to manage Todo items",
                Contact = new OpenApiContact
                {
                    Name = "Károly Ozsvárt",
                    Url = new Uri("https://www.linkedin.com/in/arphox/"),
                },
                License = new OpenApiLicense
                {
                    Name = "MIT",
                    Url = new Uri("https://choosealicense.com/licenses/mit/"),
                }
            });
        });
    }
}