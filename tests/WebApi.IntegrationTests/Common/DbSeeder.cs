﻿using Domain.Entities;
using Persistence;

namespace WebApi.IntegrationTests.Common;

public sealed class BurntInObjects
{
    public TodoItem UncompletedItem { get; init; } = default!;

    public TodoItem CompletedItem { get; init; } = default!;
}

public static class DbSeeder
{
    public static BurntInObjects Seed(TodoDbContext context)
    {
        var uncompletedItem = new TodoItem()
        {
            IsComplete = false,
            Title = "Find a goal"
        };

        var completedItem = new TodoItem()
        {
            IsComplete = true,
            Title = "Wash the dishes"
        };

        context.TodoItems.Add(uncompletedItem);
        context.TodoItems.Add(completedItem);
        context.SaveChanges();

        return new BurntInObjects
        {
            UncompletedItem = uncompletedItem,
            CompletedItem = completedItem,
        };
    }
}
