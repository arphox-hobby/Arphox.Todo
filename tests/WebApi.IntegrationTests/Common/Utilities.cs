﻿using System.Text;
using System.Text.Json;

namespace WebApi.IntegrationTests.Common;

public static class Utilities
{
    public static StringContent GetRequestContent(object obj)
    {
        return new StringContent(
            JsonSerializer.Serialize(obj),
            Encoding.UTF8,
            "application/json");
    }

    public static async Task<T> GetResponseContent<T>(HttpResponseMessage response)
    {
        var stringResponse = await response.Content.ReadAsStringAsync();
        var options = new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true
        };

        return JsonSerializer.Deserialize<T>(stringResponse, options);
    }
}
