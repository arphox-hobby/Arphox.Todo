# Arphox.Todo

Showcase of Károly Ozsvárt's work.

## Examples
Let's see some use cases and how to do them with the API.

### REST API use-cases
See the REST API at `/swagger/index.html`, but it is configured as a default launch URL too.
| Use-case | Action |
| ------ | ------ |
| Add new item | `TodoItems/Create` |
| Delete item | `TodoItems/Delete/{id}` |
| Get item | `TodoItems/Get/{id}` |
| Get **all** items | `TodoItems/GetAll` |
| Update item | `TodoItems/Update` |

### GraphQL API use-cases
Used to query data.  
Find the endpoint at the `/graphql` route.

<details>
<summary><h4>List the first 10 items</h4></summary>

```graphql
{
  todoItems(
    order: { id: ASC }
    first: 10
  ) {
    nodes {
      id
      title
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
```

</details>

<details>
<summary><h4>List the last 5 incomplete items</h4></summary>

```graphql
{
  todoItems(
    where: { isComplete: { eq: false } }
    order: { id: DESC }
    first: 5
  ) {
    nodes {
      id
      title
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
```

</details>

<details>
<summary><h4>Find items containing "a" (limit: 5, use paging)</h4></summary>

```graphql
{
  todoItems(
    where: { title: { contains: "a" } }
    order: { id: ASC }
    first: 5
  ) {
    nodes {
      id
      title
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
```

#### Same search, second page

```graphql
{
  todoItems(
    where: { title: { contains: "a" } }
    order: { id: ASC }
    first: 5
    after: "NA==" // <- You have to update this based on your first page's results!
  ) {
    nodes {
      id
      title
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
```

</details>





## Design decisions

### Working with the code

**DD0001**: For all CQRS command and query sent through the Mediator, a validator must exist.  
*Justification:* If it is not mandatory, it can be forgotten. If it is mandatory, you can still create an empty validator. I prefer getting reminders.  
*Alternative:* write a unit test that does some reflection mumbo-jumbo to find a validator for all commands/queries. I may consider implementing this in the future.

### API design

**DD0002**: PUT cannot be used to create an object.  
*Justification:* PUT has to be idempotent; which means if an id is specified in the request, it has to be respected. Creating a resource with the specified id make the default implementation of the create method faulty, since when the auto-incremented id reaches the specified id, that POST request will fail with 'duplicate key error'.  
*Alternative 1*: Make the resource id a uuid, so there is no problem at Create. Drawback: uuid is 128 bits, the current int64 is 64 bits.  
*Alternative 2*: When a create would fail with 'duplicate key error', try again. This could hurt performance, so I'd avoid it.
